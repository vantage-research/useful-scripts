%% Get filename
active = matlab.desktop.editor.getActive;
[folder,filename] = fileparts(active.Filename);

%% Make folder
backupFolderParent = [folder filesep 'backups'];
if ~exist(backupFolderParent,'dir')
    mkdir(backupFolderParent)
end

backupFolder = [folder filesep 'backups' filesep filename];
if ~exist(backupFolder,'dir')
    mkdir(backupFolder)
end

%% Filename
str = datestr(now);
str=strrep(str,'-','_');
str=strrep(str,' ','_');
str=strrep(str,':','_');
backupFileName = [backupFolder filesep filename ['_' str '.m']];

%% Save
copyfile(active.Filename,backupFileName)
h = msgbox(['Saved Backup Copy: ' backupFileName])